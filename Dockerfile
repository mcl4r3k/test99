FROM openjdk:11-alpine
ENTRYPOINT ["/usr/bin/test99.sh"]

COPY test99.sh /usr/bin/test99.sh
COPY target/test99.jar /usr/share/test99/test99.jar
